//
//  KeyNamesConstants.swift
//  Parth Hirpara
//

import Foundation
import UIKit



let APP_DELEGATE                =   UIApplication.shared.delegate as! AppDelegate
let kAppDelegate                =   UIApplication.shared.delegate as! AppDelegate
let kRootViewController         =   kAppDelegate.window?.rootViewController
let kMainStoryBoard             =   UIStoryboard(name: "Main", bundle: nil)
let kLoginStoryBoard            =   UIStoryboard(name: "Login", bundle: nil)
let USERDEFAULTS                =   UserDefaults.standard

public let defaultShimmerValue  :   Int = 10

//MARK:- Alert Button

public let kOK                      :   String  =   "Ok"
public let kCancel                  :   String  =   "Cancel"
public let kDelete                  :   String  =   "Delete"
public let kEdit                    :   String  =   "Edit"


//MARK:- APP NAME
public let APPNAME :String = "SherApp"

let loadercolor = [themeBlueColor, themeGreenColor]

//MARK:- COLOR NAMES
let APP_COLOR       =   Color_Hex(hex: "#002E5D")
let themeBlueColor  =   APP_COLOR
let themeGreenColor =   Color_Hex(hex: "#5EDA75")

struct ObserverName {
    static let kcontentSize     = "contentSize"
}

//MARK:- User Defaut
public  let kUD_UserType            :   String      =   "userType"
public  let UD_Device_Token         :   String      =   "DeviceToken"
public  let kNotificationMenuSelect :   String      =   "NotificationMenuSelect"
//public  let


//MARK:- Model
public  let kModelDocumentAttachment      :   String      =   "Document"
public  let kModelPhotoAttachment         :   String      =   "Photo"

public  let kSendMailType                 :   String      =   "1"
public  let kInboxMailType                :   String      =   "2"
public  let kSendMail                     :   String      =   "SentMail"
public  let kReplayMailKey                :   String      =   "ReplayMail"
public  let kSendRecommendation           :   String      =   "SendRecommendation"
public  let kAcceptRecommendation         :   String      =   "AcceptRecommendation"
public  let kApplyPostJob                 :   String      =   "ApplyPostJob"
public  let kSendPostJobOffer             :   String      =   "SendPostJobOffer"
public  let kAcceptPostJobOffer           :   String      =   "AcceptPostJobOffer"
public  let kRejectPostJobOffer           :   String      =   "RejectPostJobOffer"




//MARK:-
public let kImgPlaceHolder  :   String  =   "AppPlaceholder"


//MARK:- STORY BOARD NAMES
public let SB_MAIN              :   String  =   "Main"
public let SB_LOGIN             :   String  =   "Login"
public let SB_CMS               :   String  =   "CMS"


//MARK:- Model
public  let kModelUserData          :   String      =   "kModelUserData"
//MARK:- ViewControl

//Login
public let idIntroVC                :   String  =   "IntroVC"
public let idLoginVC                :   String  =   "LoginVC"
public let idSignupVC               :   String  =   "SignupVC"
public let idVerificationVC         :   String  =   "VerificationVC"
public let idForgotPasswordVC       :   String  =   "ForgotPasswordVC"
public let idSetNewPasswordVC       :   String  =   "SetNewPasswordVC"
public let idRegisteringTypeVC      :   String  =   "RegisteringTypeVC"
public let idCompliteProfileVC      :   String  =   "CompliteProfileVC"
public let idPickerVC               :   String  =   "PickerVC"

//Main
public let idMenuVC                 :   String  =   "MenuVC"
public let idDashboardVC            :   String  =   "DashboardVC"
public let idProfileVC            :   String  =   "MyProfileVC"
public let idCartVC            :   String  =   "CartVC"
public let idOrderVC          :   String  =   "OrderVC"



//CMS
public let idSupportVC              :   String  =   "SupportVC"
public let idSupportTicketListVC    :   String  =   "SupportTicketListVC"
public let idSupportChatVC          :   String  =   "SupportChatVC"
public let idAddNewTicketVC         :   String  =   "AddNewTicketVC"
public let idFAQDetailVC            :   String  =   "FAQDetailVC"
public let idFeedBackVC             :   String  =   "FeedBackVC"
public let idInviteAFriendVC        :   String  =   "InviteAFriendVC"
public let idAboutUsVC              :   String  =   "AboutUsVC"
public let idResourcesVC            :   String  =   "ResourcesVC"
public let idResourcesDetailVC      :   String  =   "ResourcesDetailVC"
public let idLenderRequestFilterVC  :   String  =   "LenderRequestFilterVC"
public let idPrivacyVC              :   String  =   "PrivacyVC"

// LogIn & SignUp
let kFileUpload_URL                 =   "/api/common/mediaUpload"

let kSignup_URL                     =   "/api/auth/signup"
let kVerification_URL               =   "/api/auth/verify"
let kResendVerification_URL         =   "/api/auth/resendVerification"
let kForfotPassord_URL              =   "/api/auth/forgotPassword"
let kForgotPasswordCheck_URL        =   "/api/auth/checkForgotCode"
let kResetPassword_URL              =   "/api/auth/resetPassword"
let kLogin_URL                      =   "/api/auth/login"
let kPrivacyPolicy_URL              =   "/api/Users/getPrivacyPolicy"
let kCompleteProfile_URL            =   "/api/Users/completeProfile"
let kUpdateProfile_URL              =   "/api/Users/updateProfile"
let kSocialLogin_URL                =   "/api/auth/socialLogin"
let kLogout_URL                     =   "/api/auth/logout"
let kGetUserInfo_URL                =   "/api/users/getUserInfo"
let kSaveUserProfile_URL            =   "/api/users/saveUserProfile"

//FAQs
let kFaq_URL                        =   "/api/common/faq"
let kFaqDetails_URL                 =   "/api/common/faqDetails"

//SUPPORT
let kSetTicket_URL                  =   "/api/common/setTicket"
let kGetTicket_URL                  =   "/api/common/getTicket"
let kReopenTicket_URL               =   "/api/common/reopenTicket"

//Country, State, City
let kGetCountryList_URL             =   "/api/common/getCountryList"
let kGetStateList_URL               =   "/api/common/getStateList"
let kGetCityList_URL                =   "/api/common/getCityList"

//FEEDBACK
let kSetAppFeedback_URL             =   "/api/common/setAppFeedback"
let kGetMyAppFeedback_URL           =   "/api/common/getMyAppFeedback"

//MARK:- Message
public let InternetNotAvailable             :   String  =   "Please connect internet."
public let RetryMessage                     :   String  =   "Something went wrong please try again..."
public let logOutConformation               :   String  =   "Are you sure to logout ?"

public let kEmptyFirstName                  :   String  =   "Please enter your first name."
public let kEmptyLastName                   :   String  =   "Please enter your last name."
public let kEmptyUserName                   :   String  =   "Please enter your user name."
public let kEmptyEmail                      :   String  =   "Please enter your email."
public let kInvalidEmail                    :   String  =   "Please enter valid email address."
public let kEmptyPassword                   :   String  =   "Please enter your password."
public let kInvalidPassword                 :   String  =   "Password length must be 8-15 chacater."
public let kAcceptTerms                     :   String  =   "Please accept terms & conditions."
public let kmsgValidationCodeEmpty          :   String  =   "Please enter verification code."
public let kEmptyConfirmPassword            :   String  =   "Please enter valid confirm password."
public let kPasswordAndConfirmNotSame       :   String  =   "Confirm password not match."
public let kEmptyOldPassword                :   String  =   "Please enter old password."
public let kLogOutMsg                       :   String  =   "Are you sure to logout?"
public let kInvalidAddress                  :   String  =   "Please enter address."

public let kEmptyProfileImage               :   String  =   "Please select profile image."
public let kEmptySSN                        :   String  =   "Please enter social security number."

//SUPPORT
public let kEmptySupportTitle               :   String  =   "Please enter support title."
public let kEmptySupportDescription         :   String  =   "Please enter support description."
public let kNoQuestion                      :   String  =   "No question!"
public let kNoPhoto                         :   String  =   "No photo!"
public let kMsgNoDataFound                  :   String  =   "No data found"

//Complete Profile
public let kMsgNoCountryFound               :   String  =   "Please select country."
public let kMsgNoStateFound                 :   String  =   "Please select state."
public let kMsgNoCityFound                  :   String  =   "Please select vity."

//FEEDBACK
public let kEmptyFeedback                   :   String  =   "Please enter feedback."
