//
//  AppData.swift
//  LoanX
//
//  Created by wm-devr on 21/11/20.
//  Copyright © 2020 wm-devr. All rights reserved.
//

import Foundation

struct AppFont {
    static let Black                =   "SFUIDisplay-Black"
    static let Bold                 =   "SFUIDisplay-Bold"
    static let Heavy                =   "SFUIDisplay-Heavy"
    static let Light                =   "SFUIDisplay-Light"
    static let Medium               =   "SFUIDisplay-Medium"
    static let Regular              =   "SFUIDisplay-Regular"
    static let Semibold             =   "SFUIDisplay-Semibold"
    static let Think                =   "SFUIDisplay-Think"
    static let Ultralight           =   "SFUIDisplay-Ultralight"
}

struct Intro {
    var image : String
    var title : String
    var desc : String
    var backImage : String
}

struct RegisterData {
    var image : String
    var title : String
    var desc : String
}

enum MenuOption {
    case Title
    case MyProfile
    case MyWallet
    case MySubscription
    case PaymentsAndBilling
    case Support
    case Feedback
    case InviteFriend
    case AboutUs
    case LogOut
    case Blank
}
