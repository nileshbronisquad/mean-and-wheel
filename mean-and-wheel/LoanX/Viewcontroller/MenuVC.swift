//
//  MenuVC.swift
//  LoanX
//
//  Created by wm-devr on 27/11/20.
//  Copyright © 2020 wm-devr. All rights reserved.
//

import UIKit

class MenuVC: UIViewController {
    
    var arrOption : [MenuOption] = [.Blank,.Blank,.Blank,.MyProfile,.MyWallet,.MySubscription,.PaymentsAndBilling,.Support,.Blank,.Blank,.Blank,.Blank,.Blank,.Feedback]
    
    @IBOutlet var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        initConfig()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    //MARK:- Init Config
    func initConfig() {
        self.tableView.registerCell(withNib: "MenuBlankTCell")
        self.tableView.registerCell(withNib: "MenuItemTCell")
        self.tableView.registerCell(withNib: "MenuSubItemTCell")

        self.tableView.delegate = self
        self.tableView.dataSource = self
    }
}
//MARK:- UITableview Delegate
extension MenuVC : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrOption.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch self.arrOption[indexPath.row] {
            
        case .MyProfile:
            let cell = tableView.dequeueReusableCell(withIdentifier: "MenuItemTCell", for: indexPath) as! MenuItemTCell
            cell.imgOption.image = UIImage(named: "gg_profile")
            cell.lblOption.text = "Profile"
            return cell
            
        case .MyWallet:
            let cell = tableView.dequeueReusableCell(withIdentifier: "MenuItemTCell", for: indexPath) as! MenuItemTCell
            cell.imgOption.image = UIImage(named: "Vector (8)")
            cell.lblOption.text = "orders"
            return cell
            
        case .MySubscription:
            let cell = tableView.dequeueReusableCell(withIdentifier: "MenuItemTCell", for: indexPath) as! MenuItemTCell
            cell.imgOption.image = UIImage(named: "Vector (8)")
            cell.lblOption.text = "My Cart"
            return cell
            
        case .PaymentsAndBilling:
            let cell = tableView.dequeueReusableCell(withIdentifier: "MenuItemTCell", for: indexPath) as! MenuItemTCell
            cell.imgOption.image = UIImage(named: "Vector (11)")
            cell.lblOption.text = "Privacy policy"
            return cell
            
        case .Support:
            let cell = tableView.dequeueReusableCell(withIdentifier: "MenuItemTCell", for: indexPath) as! MenuItemTCell
            cell.imgOption.image = UIImage(named: "whh_securityalt")
            cell.lblOption.text = "Security"
            return cell
            
        case .Feedback:
            let cell = tableView.dequeueReusableCell(withIdentifier: "MenuSubItemTCell", for: indexPath) as! MenuSubItemTCell
            
            cell.lblTitle.text = "Sign-out"
            return cell
            
            
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "MenuBlankTCell", for: indexPath) as! MenuBlankTCell
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch self.arrOption[indexPath.row] {
            
        case .MyProfile:
            let mainViewController = self.sideMenuController!
            mainViewController.hideLeftViewAnimated()
            DispatchQueue.main.async {
                let dic:[String: String] = ["value": idProfileVC]
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: kNotificationMenuSelect), object: nil, userInfo: dic)
            }
            break
            
        case .MyWallet:
            let mainViewController = self.sideMenuController!
            mainViewController.hideLeftViewAnimated()
            DispatchQueue.main.async {
                let dic:[String: String] = ["value": idOrderVC]
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: kNotificationMenuSelect), object: nil, userInfo: dic)
            }
            break
            
        case .MySubscription:
            let mainViewController = self.sideMenuController!
            mainViewController.hideLeftViewAnimated()
            DispatchQueue.main.async {
                let dic:[String: String] = ["value": idCartVC]
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: kNotificationMenuSelect), object: nil, userInfo: dic)
            }
            break
            

        case .Support:
            let mainViewController = self.sideMenuController!
            mainViewController.hideLeftViewAnimated()
            DispatchQueue.main.async {
                let dic:[String: String] = ["value": "idCartVC"]
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: kNotificationMenuSelect), object: nil, userInfo: dic)
            }
            break
            
        case .Feedback:
            let mainViewController = self.sideMenuController!
            mainViewController.hideLeftViewAnimated()
            DispatchQueue.main.async {
                let dic:[String: String] = ["value": idFeedBackVC]
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: kNotificationMenuSelect), object: nil, userInfo: dic)
            }
            break
            
        case .InviteFriend:
            let mainViewController = self.sideMenuController!
            mainViewController.hideLeftViewAnimated()
            DispatchQueue.main.async {
                let dic:[String: String] = ["value": idInviteAFriendVC]
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: kNotificationMenuSelect), object: nil, userInfo: dic)
            }
            break
            
        case .AboutUs:
            let mainViewController = self.sideMenuController!
            mainViewController.hideLeftViewAnimated()
            DispatchQueue.main.async {
                let dic:[String: String] = ["value": idAboutUsVC]
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: kNotificationMenuSelect), object: nil, userInfo: dic)
            }
            break
            
        case .PaymentsAndBilling:
            let mainViewController = self.sideMenuController!
            mainViewController.hideLeftViewAnimated()
            DispatchQueue.main.async {
                let dic:[String: String] = ["value": "idPaymentsBillingVC"]
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: kNotificationMenuSelect), object: nil, userInfo: dic)
            }
            break
            
        
        case .LogOut:
            let alt = UIAlertController(title: nil, message: kLogOutMsg, preferredStyle: .alert)
            alt.addAction(UIAlertAction(title: "Yes", style: .destructive, handler: { (action) in
                //logout()
            }))
            alt.addAction(UIAlertAction(title: "No", style: .default, handler: nil))
            self.present(alt, animated: true, completion: nil)
            let mainViewController = self.sideMenuController!
            mainViewController.hideLeftViewAnimated()
            
        default:
            print("Other")
        }
    }
}
