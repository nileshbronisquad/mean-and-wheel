//
//  DashboardVC.swift
//  LoanX
//
//  Created by wm-devr on 24/11/20.
//  Copyright © 2020 wm-devr. All rights reserved.
//

import UIKit

class DashboardVC: UIViewController {
    
    @IBOutlet weak var bgSearchView: UIView!
    @IBOutlet weak var imgFilter: UIImageView!
    @IBOutlet weak var imgMenu: UIImageView!
    @IBOutlet weak var viewShadows: UIView!
    @IBOutlet weak var containt: UIView!
    
    @IBOutlet weak var imgHome: UIImageView!
    @IBOutlet weak var imgRequest: UIImageView!
    @IBOutlet weak var imgMyLoans: UIImageView!
    
    @IBOutlet weak var lblHome: UILabel!
    @IBOutlet weak var lblLoanRequest: UILabel!
    @IBOutlet weak var lblMyLoans: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
      
        NotificationCenter.default.addObserver(self, selector: #selector(setMenuNotification(notfication:)), name: NSNotification.Name(rawValue: kNotificationMenuSelect), object: nil)
        
        self.imgMenu.applyShadowOnView()
        self.imgFilter.applyShadowOnView()
        self.bgSearchView.applyShadowOnView()
        self.bgSearchView.layer.cornerRadius = self.bgSearchView.frame.size.height / 2
       
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    //MARK:- Notification
    @objc func setMenuNotification(notfication: NSNotification) {
        
        if let value = notfication.userInfo?["value"] as? String {
            if value == idProfileVC {
                let vc: MyProfileVC = loadVC(strStoryboardId: SB_LOGIN, strVCId: idProfileVC) as! MyProfileVC
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else if value == idCartVC {
                let vc: CartVC = loadVC(strStoryboardId: SB_LOGIN, strVCId: idCartVC) as! CartVC
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else if value == idOrderVC {
                let vc: OrderVC = loadVC(strStoryboardId: SB_LOGIN, strVCId: idOrderVC) as! OrderVC
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
            
            
            
        }
        
    }
    
    @IBAction func btnFilter(_ sender: Any) {
        let vc = FoodDetailVC.initVC()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func btnMenu(_ sender: Any) {
        
        self.sideMenuController?.showLeftViewAnimated()
    }
    
}
