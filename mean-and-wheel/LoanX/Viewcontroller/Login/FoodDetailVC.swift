//
//  FoodDetailVC.swift
//  LoanX
//
//  Created by iMac on 13/03/21.
//  Copyright © 2021 wm-devr. All rights reserved.
//

import UIKit

class FoodDetailVC: UIViewController {
    
    
    @IBOutlet weak var btnBeverages: UIButton!
    @IBOutlet weak var btnNonVeg: UIButton!
    @IBOutlet weak var btnVeg: UIButton!
    @IBOutlet weak var btnBestSellar: UIButton!
    @IBOutlet weak var viewFav: UIView!
    @IBOutlet weak var viewShare: UIView!
    @IBOutlet weak var viewBack: UIView!
    
    // MARK:- Create self View Controller object
    class func initVC() -> FoodDetailVC {
        let vc = kLoginStoryBoard.instantiateViewController(withIdentifier: "FoodDetailVC") as! FoodDetailVC
        vc.modalPresentationStyle = .overCurrentContext
        return vc
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        self.viewBack.applyShadowOnView()
        self.viewFav.applyShadowOnView()
        self.viewShare.applyShadowOnView()
        
        self.btnVeg.myViewCorners()
        self.btnNonVeg.myViewCorners()
        self.btnBeverages.myViewCorners()
        self.btnBestSellar.myViewCorners()
        
        
    }
    
}
