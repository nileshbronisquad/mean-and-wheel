//
//  HistoryVC.swift
//  mean-and-wheel
//
//  Created by iMac on 11/03/21.
//

import UIKit

class HistoryVC: UIViewController {
    
    @IBOutlet weak var btnBackView: UIView!
    @IBOutlet weak var btnUpdate: UIButton!
    
    // MARK:- Create self View Controller object
    class func initVC() -> HistoryVC {
        let vc = kLoginStoryBoard.instantiateViewController(withIdentifier: "HistoryVC") as! HistoryVC
        vc.modalPresentationStyle = .overCurrentContext
        return vc
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.btnBackView.applyShadowOnView()

        self.btnUpdate.layer.masksToBounds = true
        self.btnUpdate.layer.cornerRadius = self.btnUpdate.frame.size.height / 2
    }
    

    @IBAction func btnUpdate(_ sender: Any) {
        let vc = OrderVC.initVC()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
