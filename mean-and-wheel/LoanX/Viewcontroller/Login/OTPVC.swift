//
//  OTPVC.swift
//  mean-and-wheel
//
//  Created by iMac on 11/03/21.
//

import UIKit
import OTPFieldView

class OTPVC: UIViewController {
    
    @IBOutlet weak var btnResend: UIButton!
    @IBOutlet weak var viewOTP: OTPFieldView!
    @IBOutlet weak var lblMobileNumber: UILabel!
    @IBOutlet weak var btnSubmit: UIButton!
    
    var strOTP = String()
    var count = 60  // 60sec if you want
    var resendTimer = Timer()
    var strMobile = ""
    @IBOutlet weak var lblDownTimer: UILabel!
    
    // MARK:- Create self View Controller object
    class func initVC() -> OTPVC {
        let vc = kLoginStoryBoard.instantiateViewController(withIdentifier: "OTPVC") as! OTPVC
        vc.modalPresentationStyle = .overCurrentContext
        return vc
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        self.startTimer()
        self.setupOtpView()
        self.btnSubmit.layer.masksToBounds = true
        self.btnSubmit.layer.cornerRadius = self.btnSubmit.frame.size.height / 2
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        self.resendTimer.invalidate()
    }
    
    func startTimer() {
        
        resendTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(update), userInfo: nil, repeats: true)
    }
    
    @objc func update() {
        if(count > 0) {
            count = count - 1
            print(count)
            self.btnResend.isEnabled = false
            self.btnResend.setTitleColor(#colorLiteral(red: 0.9142372012, green: 0.3701301813, blue: 0.3032737374, alpha: 1), for: .normal)
            self.lblDownTimer.isHidden = false
            self.lblDownTimer.text = "\(count)"
        }
        else {
            resendTimer.invalidate()
            print("call your api")
            self.btnResend.isEnabled = true
            self.btnResend.setTitleColor(#colorLiteral(red: 0.9142372012, green: 0.3701301813, blue: 0.3032737374, alpha: 1), for: .normal)
            self.lblDownTimer.isHidden = true
        }
    }

    @IBAction func btnSubmit(_ sender: Any) {
        let vc = SignUPVC.initVC()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func setupOtpView(){
        
        self.viewOTP.fieldsCount = 4
        self.viewOTP.fieldBorderWidth = 2
        self.viewOTP.defaultBorderColor = #colorLiteral(red: 0.8392156863, green: 0.8392156863, blue: 0.8392156863, alpha: 1)
        self.viewOTP.applyShadowOnView()
        self.viewOTP.filledBorderColor = #colorLiteral(red: 0.1568627451, green: 0.1843137255, blue: 0.2235294118, alpha: 1)
        self.viewOTP.cursorColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.viewOTP.displayType = .square
        self.viewOTP.fieldSize = 40
        self.viewOTP.separatorSpace = 20
        self.viewOTP.shouldAllowIntermediateEditing = false
        self.viewOTP.delegate = self
        self.viewOTP.initializeUI()
        
    }
   

}


extension OTPVC : OTPFieldViewDelegate {
    func shouldBecomeFirstResponderForOTP(otpTextFieldIndex index: Int) -> Bool {
        return true
    }
    
    func enteredOTP(otp: String) {
        self.strOTP = otp
    }
    
    func hasEnteredAllOTP(hasEnteredAll: Bool) -> Bool {
        if hasEnteredAll {
            
        }
        else {
            return false
        }
        return false
        
    }

    
}
