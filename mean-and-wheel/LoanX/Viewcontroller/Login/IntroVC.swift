//
//  IntroVC.swift
//  LoanX
//
//  Created by wm-devr on 21/11/20.
//  Copyright © 2020 wm-devr. All rights reserved.
//

import UIKit

class IntroVC: UIViewController {

    @IBOutlet weak var btnSkip: UIButton!
    @IBOutlet weak var page: UIPageControl!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var arrData = [Intro]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        
        self.setup()
    }
    
    //MARK:- Custom Method
    func setup() {
        let intro1:Intro = Intro(image: "popcorn 1", title: "Choose Your Meal", desc: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy", backImage: "introSCBack1")
        arrData.append(intro1)
        
        let intro2:Intro = Intro(image: "money 1", title: "Choose Your Payment", desc: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy", backImage: "introSCBack2")
        arrData.append(intro2)
        
        let intro3:Intro = Intro(image: "restaurant 1", title: "Enjoy Your Food", desc: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy", backImage: "introSCBack3")
        arrData.append(intro3)
        
        self.collectionView.reloadData()
    }
    
    //MARK:- Button Click
    
    @IBAction func btnSkipClick(_ sender: Any) {
        let vc = LoginVC.initVC()
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension IntroVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell : IntroCCell = collectionView.dequeueReusableCell(withReuseIdentifier: "IntroCCell", for: indexPath) as! IntroCCell
        let data = arrData[indexPath.row]
        cell.imgMain.image = UIImage(named: data.image)
        cell.lblTitle.text = data.title
        cell.lblDesc.text = data.desc
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        let data = self.arrData[indexPath.row]
        self.page.currentPage = indexPath.row
        if indexPath.row == 0 {
            self.btnSkip.setTitle("Skip", for: .normal)
        }
        else if indexPath.row == 1 {
            self.btnSkip.setTitle("Skip", for: .normal)
        }
        else {
            self.btnSkip.setTitle("Next", for: .normal)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
    }
}

class IntroCCell : UICollectionViewCell {
    
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgMain: UIImageView!
}
