//
//  CartVC.swift
//  mean-and-wheel
//
//  Created by iMac on 11/03/21.
//

import UIKit
import SwipeCellKit

class CartVC: UIViewController,UITableViewDelegate, UITableViewDataSource,SwipeTableViewCellDelegate {
    
    @IBOutlet weak var btnUpdate: UIButton!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var btnBackView: UIView!
    
    // MARK:- Create self View Controller object
    class func initVC() -> CartVC {
        let vc = kLoginStoryBoard.instantiateViewController(withIdentifier: "CartVC") as! CartVC
        vc.modalPresentationStyle = .overCurrentContext
        return vc
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        tblView.registerCell(withNib: "CartCell")
        self.tblView.reloadData()
        self.tblView.separatorStyle = .none
        
        self.btnBackView.applyShadowOnView()

        self.btnUpdate.layer.masksToBounds = true
        self.btnUpdate.layer.cornerRadius = self.btnUpdate.frame.size.height / 2
        
    }
    

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 10
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : CartCell = tableView.dequeueReusableCell(withIdentifier: "CartCell") as! CartCell
        
//        cell.btnPlus.tag = indexPath.row + 5000
//        cell.btnPlus.addTarget(self, action: #selector(self.btnPlus(_:)), for: .touchUpInside)
//
//        cell.btnMinus.tag = indexPath.row
//        cell.btnMinus.addTarget(self, action: #selector(self.btnMinus(_:)), for: .touchUpInside)
        
        cell.delegate = self
       
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 115
        
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        
        guard orientation == .right else { return nil }
        
        let deleteAction = SwipeAction(style: .destructive, title: "") { action, indexPath in
            //self.callWSForGetremoveConnection(for: indexPath)
        }
        
        let favAction = SwipeAction(style: .destructive, title: "") { action, indexPath in
            //self.callWSForGetremoveConnection(for: indexPath)
        }
        
        // customize the action appearance
        deleteAction.backgroundColor = #colorLiteral(red: 0.9607843137, green: 0.9607843137, blue: 0.9725490196, alpha: 1)
        deleteAction.image = UIImage(named: "Group 129")
        
        // customize the action appearance
        favAction.backgroundColor = #colorLiteral(red: 0.9607843137, green: 0.9607843137, blue: 0.9725490196, alpha: 1)
        favAction.image = UIImage(named: "Group 128")
        
        return [deleteAction,favAction]
    }
    
    @objc func btnPlus(_ sender: UIButton)
    {
        let point = sender.convert(CGPoint.zero, to: tblView)
        if let indexPath = tblView.indexPathForRow(at: point) {
            
//            let cell = tblView.cellForRow(at: indexPath) as? CusineChoiceCell
//
//            let value = Int(cell?.lblValue.text! ?? "1")
//
//            if value! > 1 {
//
//                cell?.lblValue.text = "\(value! - 1)"
//
//                self.tblView.reloadRows(at: [indexPath], with:.none)
//
//                self.tblView.reloadData()
            }
            
        }
       
    
    
    @objc func btnMinus(_ sender: UIButton)
    {
        let point = sender.convert(CGPoint.zero, to: tblView)
        if let indexPath = tblView.indexPathForRow(at: point) {
           
//            let cell = tblView.cellForRow(at: indexPath) as? CusineChoiceCell
//
//            let value = Int(cell?.lblValue.text! ?? "0")
//
//            cell?.lblValue.text = "\(value! + 1)"
//
//            self.tblView.reloadRows(at: [indexPath], with:.none)
//
//            self.tblView.reloadData()
        }
        
    }
   
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnUpdate(_ sender: Any) {
        let vc = CheckOutVC.initVC()
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

// MARK:-
extension UITableView {
    func registerCell(withNib reuseIdentifier:String) {
        self.register(UINib(nibName: reuseIdentifier, bundle: Bundle.main), forCellReuseIdentifier: reuseIdentifier)
    }
}
