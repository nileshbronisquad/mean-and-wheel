//
//  MyProfileVC.swift
//  mean-and-wheel
//
//  Created by iMac on 11/03/21.
//

import UIKit

class MyProfileVC: UIViewController {
    
    
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var view4: UIView!
    @IBOutlet weak var view3: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var btnBackView: UIView!
    
    @IBOutlet weak var profileBgView: UIView!
    
    // MARK:- Create self View Controller object
    class func initVC() -> MyProfileVC {
        let vc = kLoginStoryBoard.instantiateViewController(withIdentifier: "MyProfileVC") as! MyProfileVC
        vc.modalPresentationStyle = .overCurrentContext
        return vc
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        self.btnBackView.applyShadowOnView()
        self.profileBgView.myViewCorners()
        
        self.view1.myViewCorners()
        self.view2.myViewCorners()
        self.view3.myViewCorners()
        self.view4.myViewCorners()
        
        self.btnSubmit.layer.masksToBounds = true
        self.btnSubmit.layer.cornerRadius = self.btnSubmit.frame.size.height / 2
        
        
    }
    
    @IBAction func btnOrder(_ sender: Any) {
        let vc = HistoryVC.initVC()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnSubmit(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
