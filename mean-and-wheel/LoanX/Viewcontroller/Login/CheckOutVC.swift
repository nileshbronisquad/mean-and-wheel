//
//  CheckOutVC.swift
//  LoanX
//
//  Created by iMac on 13/03/21.
//  Copyright © 2021 wm-devr. All rights reserved.
//

import UIKit

class CheckOutVC: UIViewController {
    
    @IBOutlet weak var bgAlertMainView: UIView!
    @IBOutlet weak var bgAlertView: UIView!
    @IBOutlet weak var btnTrackOrder: UIButton!
    @IBOutlet weak var bgBackView: UIView!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var btnUpdate: UIButton!
    
    // MARK:- Create self View Controller object
    class func initVC() -> CheckOutVC {
        let vc = kLoginStoryBoard.instantiateViewController(withIdentifier: "CheckOutVC") as! CheckOutVC
        vc.modalPresentationStyle = .overCurrentContext
        return vc
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.bgAlertMainView.isHidden = true

        self.bgView.applyShadowOnView()
        self.bgBackView.applyShadowOnView()
        
        self.btnUpdate.layer.masksToBounds = true
        self.btnUpdate.layer.cornerRadius = self.btnUpdate.frame.size.height / 2
        
        self.btnTrackOrder.myViewCorners()
        self.bgAlertView.myViewCorners()
        self.bgAlertView.applyShadowOnView()
        
    }
    

    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func btnPayment(_ sender: Any) {
        self.bgAlertMainView.isHidden = false
    }
    
    @IBAction func btnTrackOrder(_ sender: Any) {
        self.bgAlertMainView.isHidden = true
    }
    @IBAction func btnClose(_ sender: Any) {
        self.bgAlertMainView.isHidden = true
    }
    
}
