//
//  LoginVC.swift
//  mean-and-wheel
//
//  Created by iMac on 11/03/21.
//

import UIKit

class LoginVC: UIViewController {
    
    @IBOutlet weak var btnLogin: UIButton!
    
    @IBOutlet weak var viewFb: UIView!
    @IBOutlet weak var viewGoogle: UIView!
    @IBOutlet weak var txtMob: UITextField!
    
    // MARK:- Create self View Controller object
    class func initVC() -> LoginVC {
        let vc = kLoginStoryBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        vc.modalPresentationStyle = .overCurrentContext
        return vc
    }


    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.btnLogin.layer.masksToBounds = true
        self.btnLogin.layer.cornerRadius = self.btnLogin.frame.size.height / 2
        
        self.viewFb.applyShadowOnView()
        self.viewGoogle.applyShadowOnView()
        
    }
    
    @IBAction func btnLogin(_ sender: Any) {
        
        let vc = OTPVC.initVC()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
  
}


extension UIView {
    //If you want only round corners
    func myViewCorners() {
        layer.cornerRadius = 10
        layer.masksToBounds = true
    }
    
    //If you want complete round shape, enable above comment line
    func myViewCorners(width:CGFloat) {
        layer.cornerRadius = width/2
        layer.borderWidth = 1.0
        layer.borderColor = UIColor.red.cgColor
        layer.masksToBounds = true
    }
    
    func applyShadowOnView() {
        layer.cornerRadius = 10
        layer.shadowColor = #colorLiteral(red: 0.7411764706, green: 0.7411764706, blue: 0.7411764706, alpha: 1)
        layer.shadowOpacity = 1
        layer.shadowOffset = .zero
        layer.shadowRadius = 5
    }
}
