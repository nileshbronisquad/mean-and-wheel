//
//  SignUPVC.swift
//  mean-and-wheel
//
//  Created by iMac on 11/03/21.
//

import UIKit

class SignUPVC: UIViewController {
    
    
    @IBOutlet weak var btnSignUP: UIButton!
    
    
    // MARK:- Create self View Controller object
    class func initVC() -> SignUPVC {
        let vc = kLoginStoryBoard.instantiateViewController(withIdentifier: "SignUPVC") as! SignUPVC
        vc.modalPresentationStyle = .overCurrentContext
        return vc
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        self.btnSignUP.layer.masksToBounds = true
        self.btnSignUP.layer.cornerRadius = self.btnSignUP.frame.size.height / 2
    }
    

    @IBAction func btnSignUP(_ sender: Any) {
        
        kAppDelegate.setMenu()
         
       
    }
    

}
