//
//  AppDelegate.swift
//  LoanX
//
//  Created by wm-devr on 20/11/20.
//  Copyright © 2020 wm-devr. All rights reserved.
//

import UIKit
import LGSideMenuController
import IQKeyboardManagerSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var DashboardBottomInset = 100
    var window: UIWindow?
    var kUserRole = "2"
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        self.window = UIWindow(frame: UIScreen.main.bounds)
        IQKeyboardManager.shared.enable = true
       
        //self.setLoginMenu()
        self.setMenu()
        return true
    }
    
    
    
}

extension AppDelegate {
    
    func setLoginMenu() {
        let vc = loadVC(strStoryboardId: SB_LOGIN, strVCId: idIntroVC)
        let nav = UINavigationController(rootViewController: vc)
        nav.setNavigationBarHidden(true, animated: false)
        kAppDelegate.window?.rootViewController = nav
        kAppDelegate.window?.makeKeyAndVisible()
    }
    
    func setMenu() {
        
        let vc = kMainStoryBoard.instantiateViewController(withIdentifier: "NavigationController")
        let mainViewController = kMainStoryBoard.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
        mainViewController.rootViewController = vc

        mainViewController.setup(type: UInt(4))
        kAppDelegate.window!.rootViewController = mainViewController
        kAppDelegate.window?.makeKeyAndVisible()
        UIView.transition(with: kAppDelegate.window!, duration: 0.3, options: [.transitionCrossDissolve], animations: nil, completion: nil)
    }
}
