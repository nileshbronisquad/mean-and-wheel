//
//  CartCell.swift
//  mean-and-wheel
//
//  Created by iMac on 11/03/21.
//

import UIKit
import SwipeCellKit

class CartCell: SwipeTableViewCell {

    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var imgFood: UIImageView!
    @IBOutlet weak var viewIncrement: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
        self.bgView.myViewCorners()
        
        self.viewIncrement.layer.masksToBounds = true
        self.viewIncrement.layer.cornerRadius = self.viewIncrement.frame.size.height / 2
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
