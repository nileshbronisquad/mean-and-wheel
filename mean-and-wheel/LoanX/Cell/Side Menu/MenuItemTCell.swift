//
//  MenuItemTCell.swift
//  LoanX
//
//  Created by APPLE on 30/11/20.
//  Copyright © 2020 wm-devr. All rights reserved.
//

import UIKit

class MenuItemTCell: UITableViewCell {

    @IBOutlet var imgOption: UIImageView!
    @IBOutlet var lblOption: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
